# Real User Monitoring (RUM) by Site24x7

https://www.site24x7.com/real-user-monitoring.html

Real User Monitoring by Site24x7 gives accurate insight into real users’ application experience and helps visualise web
app interaction patterns. Real User Monitoring provides deep insight into key performance metrics right from the
initiation of the URL until the request is served back to the browser.  

This Site24x7 RUM module helps you add your Site24x7’s RUM code snippet to the HEAD tag of your Drupal website. Once
added, Site24x7 immediately starts collecting data from the site visitors. You can view all that collected data in your
Site24x7 console at [https://www.site24x7.com/app/apm#/apm/rum/list/](https://www.site24x7.com/app/apm#/apm/rum/list/).

Please note that you need a [Site24x7 account](https://www.site24x7.com/signup.html?pack=4&l=en) for this plugin. If you
don’t have one, grab one at [site24x7.com](https://www.site24x7.com/signup.html?pack=4&l=en)!

### Steps to get RUM Key

* Log-in to your [Site24x7](https://www.site24x7.com/login.html) account.
* Go to APM tab -> Web RUM -> Add Application and enter an Application Name. Click Save to continue.
* Copy the RUM key from the Web RUM script and paste it above.
* Look for something like this: rumMOKey='7dh8cxiw6xxxze2nrf6driuwd'
* In this example above, the RUM key is: _7dh8cxiwxxx6ze2nrf6driuwd_
* Refer [this user guide](https://www.site24x7.com/help/apm/rum.html) for more information.
